#!/bin/sh
set -e
# https://coreos.com/kubernetes/docs/latest/openssl.html

# https://coreos.com/os/docs/latest/generate-self-signed-certificates.html
#
# we are going to generate admin, ca certs first.
# and by using ca, we generate server, client-server, client role certs
# generating server and client-server roles certs is called
# in crate_droplet.sh - work_on_droplet ()

# path=$1
# path=`echo ${path/\/\/\//\/}`

path="./"

rm $path/certs/generated/*

#=======================================
# ROOT CA PUBLIC KEY
#=======================================

echo '{"CN":"ca","key":{"algo":"rsa","size":4096}}' | \
  $path/certs/cfssl gencert -initca -  | \
  $path/certs/cfssljson -bare $path/certs/generated/ca -

template=`cat "$path/certs/ca-config-template.json"`
echo ${template} > "$path/certs/generated/ca-config.json"

#=======================================
# Verify data
#=======================================
# openssl x509 -in ca.pem -text -noout
# openssl x509 -in server.pem -text -noout
# openssl x509 -in client.pem -text -noout


echo "#======================================="
echo "# For kubernetes admin"
echo "#======================================="

echo '{"CN":"admin","key":{"algo":"rsa","size":4096}}' | \
  $path/certs/cfssl gencert -initca - | \
  $path/certs/cfssljson -bare $path/certs/generated/admin -


#=======================================
# client
#=======================================
# client certificate is used to authenticate client by server.
# For example etcdctl, etcd proxy, fleetctl or docker clients.

echo '{"CN":"client","hosts":[""],"key":{"algo":"rsa","size":4096}}' | \
$path/certs/cfssl gencert \
-ca=$path/certs/generated/ca.pem \
-ca-key=$path/certs/generated/ca-key.pem \
-config=$path/certs/generated/ca-config.json \
-profile=client - | \
$path/certs/cfssljson -bare $path/certs/generated/client


#=======================================
# API Server
#=======================================
# server certificate is used by server and verified by
# client for server identity. For example docker server
# or kube-apiserver

echo '{"CN":"server","hosts":[""],"key":{"algo":"rsa","size":4096}}' | \
$path/certs/cfssl gencert \
-ca=$path/certs/generated/ca.pem \
-ca-key=$path/certs/generated/ca-key.pem \
-config=$path/certs/generated/ca-config.json \
-profile=server \
-hostname="master.mugen.tech" - | \
$path/certs/cfssljson -bare $path/certs/generated/server

#=======================================
# Worker Node
#=======================================
# client-server certificate is used by etcd cluster members
# as they communicate with each other in both ways.

echo '{"CN":"worker","hosts":[""],"key":{"algo":"rsa","size":4096}}' | \
$path/certs/cfssl gencert \
-ca=$path/certs/generated/ca.pem \
-ca-key=$path/certs/generated/ca-key.pem \
-config=$path/certs/generated/ca-config.json \
-profile=client-server \
-hostname="mugen.tech" - | \
$path/certs/cfssljson -bare $path/certs/generated/worker

