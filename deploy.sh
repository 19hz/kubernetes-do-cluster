#!/bin/sh
set -e

make discovery-url
make generate_certs

# terraform apply -var "do_token=$PROJECT_19HZ_DO_TOKEN" \
#                 -var "ssh_fingerprint=$PROJECT_19HZ_SSH_FINGERPRINT" \
#                 -var "pub_key=$PUB_KEY" \
#                 -var "pvt_key=$PVT_KEY" \
#                 -var "size=8GB" \
#                 -var "prefix=mugen" \
#                 -var "region=sgp1"
