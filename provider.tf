variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}
variable "size" {}
variable "region" {}
variable "prefix" {}

provider "digitalocean" {
  token = "${var.do_token}"
}
