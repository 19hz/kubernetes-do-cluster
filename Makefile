#
# Makefile
#

SHELL = /bin/bash

ifndef NUM_INSTANCES
  NUM_INSTANCES = 3
endif

discovery-url:
	@for i in 1 2 3 4 5; do \
		URL=`curl -s -w '\n' https://discovery.etcd.io/new?size=$$NUM_INSTANCES`; \
		if [ ! -z $$URL ]; then \
			sed -e "s,discovery: #DISCOVERY_URL,discovery: $$URL," kubernetes-digitalocean-master.yaml.example > kubernetes-digitalocean-master.yaml; \
			sed -e "s,discovery: #DISCOVERY_URL,discovery: $$URL," kubernetes-digitalocean-node.yaml.example > kubernetes-digitalocean-node.yaml; \
			echo "Wrote $$URL to yaml files"; \
		    break; \
		fi; \
		if [ $$i -eq 5 ]; then \
			echo "Failed to contact https://discovery.etcd.io after $$i tries"; \
		else \
			sleep 3; \
		fi \
	done

generate_certs:
	certs/generate_certs.sh
