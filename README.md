# TODO

https://coreos.com/kubernetes/docs/latest/getting-started.html

NEED TO CHECK KUBE CONFIG LIKE THIS
```
apiVersion: v1
kind: Config
clusters:
- name: local
  cluster:
    certificate-authority: /etc/kubernetes/ssl/ca.pem
users:
- name: kubelet
  user:
    client-certificate: /etc/kubernetes/ssl/server.pem
    client-key: /etc/kubernetes/ssl/server-key.pem
contexts:
- context:
    cluster: local
    user: kubelet
  name: kubelet-context
current-context: kubelet-context

```

# kubernetes-coreos-digitalocean

Kubernetes on CoreOS on Digitalocean!

```
ssh-keygen -q -t rsa -f ~/.ssh/19hz -N '' -C 19hz
ssh-add ~/.ssh/19hz

make discovery-url

export PROJECT_19HZ_SSH_FINGERPRINT="finger print on do"
export PROJECT_19HZ_DO_TOKEN="your token"
export PROJECT_19HZ_PUB_KEY="/Users/jaigoukkim/.ssh/19hz.pub"
export PROJECT_19HZ_PVT_KEY="/Users/jaigoukkim/.ssh/19hz"

terraform apply -var "do_token=$PROJECT_19HZ_DO_TOKEN" \
                -var "ssh_fingerprint=$PROJECT_19HZ_SSH_FINGERPRINT" \
                -var "pub_key=$PUB_KEY" \
                -var "pvt_key=$PVT_KEY" \
                -var "size=8GB" \
                -var "prefix=mugen" \
                -var "region=sgp1"
```


setup DNS manually on digitalocean and then setup firewall


currently firewall is not implemented.

```
for i in 1 2 3; do ssh -i ~/.ssh/19hz core@mugen-0$i.mugen.tech 'bash -s' < ./custom-firewall.sh; done
```

### deploy

wget https://github.com/kubernetes/kubernetes/releases/download/v1.1.8/kubernetes.tar.gz

tar -xzvf kubernetes.tar.gz
cp kubernetes/platforms/darwin/amd64/kubectl /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl

https://coreos.com/kubernetes/docs/latest/configure-kubectl.html

### Ref

https://www.livewyer.com/blog/2015/05/20/deploying-kubernetes-digitalocean

https://github.com/deis/deis/tree/master/contrib/digitalocean
